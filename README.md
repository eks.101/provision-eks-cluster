# Provision an EKS Cluster (AWS)

This repo was based on [Provision an EKS Cluster tutorial](https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks), 
This repo contains Terraform configuration files to provision an EKS cluster on AWS.


.

## Prerequisites
  - AWS account
  - AWS Cli configured
  - Kubectl configured
  - checkov
  - If you want to run the pipeline, need to have a gitlab account configured


## Steps to reproduce the project

terraform init
terraform plan
terraform apply

## Variables that must be changed


## Cluster Validation 


